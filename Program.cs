﻿using System;
using System.ServiceModel;

namespace Client
{
    [ServiceContract]
    public interface IMyService
    {
        [OperationContract]
        double GetSum(double i, double j);

        [OperationContract]
        double GetMult(double i, double j);

        [OperationContract]
        double GetSubtr(double i, double j);

        [OperationContract]
        double GetDiv(double i, double j);
    }

    class Program
    {
        static void Main(string[] args)
        {            
            var tcpUri = new Uri("http://localhost:8000/MyService");
            
            var address = new EndpointAddress(tcpUri);
            var binding = new BasicHttpBinding();
            
            var factory = new ChannelFactory<IMyService>(binding, address);
            // 
            IMyService service = factory.CreateChannel();
            Console.WriteLine(service.GetSum(3, 5));
            Console.WriteLine(service.GetSum(5, 12));
            Console.WriteLine(service.GetMult(3, 5));
            Console.WriteLine(service.GetMult(-3, 15));
            Console.ReadLine();
        }
    }
}